Solar Activity is the biggest contributor towards the depleting Space Weather. Considering
multiple parameters as the cause of solar eruptions, many studies have been done in this field,
predicting the solar flare events for future. Taking the spotted sunspots and occurrences of
solar flares data from 1981 to 2002, this report analyses the cause of solar flares and tries
to create a relationship between the the characteristics of sunspots and occurrences of solar
flares from them. After the analysis, it was concluded that the classification of a sunspot
group plays a huge role in understanding its flaring probability. Unipolar Sunspot groups are
the weakest, but bipolar sunspot group with multiple strong sunspots packed closely are the
strongest and produce the strongest solar flares.

2 different datasets, one for solar flares and another for sunspots were taken and merged analysing the date and time of occurences and the NOAA
numbers. The algorithm is explained in the report properly. The report uses KNIME and Tableau to create the dataset and to perform visualisations 
respectively.